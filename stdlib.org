#+OPTIONS: toc:nil num:nil <:nil \n:nil ::nil timestamp:nil *:nil ':nil
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="style.css"/>
#+EXPORT_FILE_NAME: stdlib.html
#+TITLE: GRU DeerTree standard library specifications

* TODO Introduction

{} - optional argument to function (FIXME: should be defined in main specfications)

* TODO Buitlin syntax

* TODO ~<def>~ header

FIXME: Like stddef.h in C, maybe more

** Types
*** ~Size~ - alias for unsigned long long

* TODO ~<io>~ header
** Types
*** ~File~ - an object type used for streams.

** Functions
*** ~printf();~ // C-like formatting
*** ~fprintf();~
*** ~sprintf();~
*** ~eprintf();~

* TODO ~<string>~ header
** Types
** Functions
*** ~isalnum()~
*** ~isalpha()~
*** ~isascii()~
*** ~isblank()~
*** ~iscntrl()~ // FIXME: maybe not needed
*** ~isdigit()~
*** ~isgraph()~ // FIXME: maybe not needed
*** ~islower()~
*** ~isprint()~ // FIXME: maybe not needed
*** ~ispunct()~
*** ~isspace()~
*** ~isupper()~
*** ~isxdigit()~
*** ~toascii()~ // FIXME: maybe not needed
*** ~tolower()~
*** ~toupper()~

* TODO ~<mem>~ header
Functions for memory usage (FIXME: better description)

** Types
** Functions
*** fn alloc(Size sz) (byte[]);
*** fn free(byte[] ptr);

* TODO ~<sys>~ header
** Types
*** Flag
** Functions
*** fn open(Path p, Flag flags) (File); // FIXME: should path be path_t or string? should flags be flag_t or string?
*** fn read(File f, Size sz) (byte[]); // FIXME: maybe it should return something like byte[]
*** fn write(File f, String str, {Size sz}) (Size);
*** fn close(File f) (int);

** TODO ~<sys/env>~ header

** TODO ~<sys/signal>~ header

** TODO ~<sys/path>~ header

** TODO ~<sys/syscall>~ header
*** Types
*** Functions
**** fn syscall(long number, {arguments}) (long); // FIXME

* TODO ~<time>~ header

* TODO ~<random>~ header

* TODO ~<regex>~ header

* TODO ~<math>~ header

* TODO ~<error>~ header
** Types
*** Err // FIXME: maybe move to builtins
** Functions

* TODO ~<num>~ header (maybe other name would be better)
For bigger integers and floats

** Types
*** int8
*** int16
*** int32
*** int64

*** uint8
*** uint16
*** uint32
*** uint64

*** float8
*** float16
*** float32
*** float64

** Functions

* TODO ~<net>~ header (maybe should be in other library)

